#PUBLIC and PRIVATE SUBNET

resource "aws_subnet" "PRA-PUBsubnet" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = var.demo-subnet-public-1_cidr
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "PRA-PUBsubnet"
  }
}

# Creating Private Subnet for database
resource "aws_subnet" "private_subnet" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = var.demo-subnet-private-1_cidr

  tags = {
    Name = "Terraform-PVT"
  }
}
