resource "aws_launch_configuration" "web" {
  name_prefix   = "web-"
  image_id      = var.image_id # Amazon Linux 2 AMI (HVM), SSD Volume Type
  instance_type = var.instance_type
  key_name      = var.key_name


  security_groups             = [aws_security_group.asg-sg.id]
  associate_public_ip_address = true




  lifecycle {
    create_before_destroy = true
  }


}
resource "aws_autoscaling_group" "web" {
  name = "${aws_launch_configuration.web.name}-asg"


  min_size         = var.min_size
  desired_capacity = var.desired_capacity
  max_size         = var.max_size



  launch_configuration = aws_launch_configuration.web.name


  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]


  metrics_granularity = "1Minute"


  vpc_zone_identifier = [
    var.subnet_id,
    var.subnet2_id
  ]


  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {

    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }
}
##### ASG Security Group  ######
resource "aws_security_group" "asg-sg" {
  name        = "TF-asg-sg2"
  description = "ASG security group"
  vpc_id      = var.vpc_id



  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["27.59.230.138/32"]
  }



  ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["165.225.122.101/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "TF-LB-Security-Group"
  }
}


###################################PRIVATE ASG####################################################
resource "aws_launch_configuration" "pri" {
  name_prefix   = "pri-"
  image_id      = var.image1_id # Amazon Linux 2 AMI (HVM), SSD Volume Type
  instance_type = var.instance_type
  key_name      = var.key_name


  security_groups             = [aws_security_group.asg-sg1.id]
  associate_public_ip_address = true




  lifecycle {
    create_before_destroy = true
  }


}

resource "aws_autoscaling_group" "pweb" {
  name = "${aws_launch_configuration.pri.name}-asg"


  min_size         = var.min_size
  desired_capacity = var.desired_capacity
  max_size         = var.max_size



  launch_configuration = aws_launch_configuration.pri.name


  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]


  metrics_granularity = "1Minute"


  vpc_zone_identifier = [
    var.subnet3_id,
    var.subnet4_id
  ]


  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {

    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }
}
##### ASG Security Group  ######
resource "aws_security_group" "asg-sg1" {
  name        = "TF-asg-sg1"
  description = "ASG security group"
  vpc_id      = var.vpc_id



  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["27.59.230.138/32"]
  }



  ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["165.225.122.101/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "TF-LB-Security-Group2"
  }
}


