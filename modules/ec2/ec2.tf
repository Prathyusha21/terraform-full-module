resource "aws_instance" "web" {
  ami             = var.ami
  instance_type   = var.instance_type
  subnet_id       = var.subnet_id
   vpc_security_group_ids = [aws_security_group.ec2-sg.id]
  key_name        = "PRA"
  tags = {
    Name = "PRA-EC2-Task"
  }
}
#### AMI Creation for Nginx Instance#####
resource "aws_ami_from_instance" "example" {
 name               = "TF-PRA-EC2-Test-AMI"
 source_instance_id = aws_instance.web.id
}
##### EC2 Security Group  ######
resource "aws_security_group" "ec2-sg" {
  name        = "Demo-ec2-sg"
  description = "EC2 security group"
  vpc_id      = var.vpc_id



  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["27.59.230.138/32"]
  }



  ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["165.225.122.101/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }



  tags = {
    Name = "EC2-Security-Group"
  }
}


##############################################PRIVATE INSTANCE-AMI-SG##############################

resource "aws_instance" "pri" {
  ami             = var.ami
  instance_type   = var.instance_type
  subnet_id       = var.subnet3_id
   vpc_security_group_ids = [aws_security_group.ec2-sg.id]
  key_name        = "PRA"
  tags = {
    Name = "TF-PRA-EC2-PRI"
  }
}
#### AMI Creation for wordpress private Instance#####
resource "aws_ami_from_instance" "example1" {
 name               = "TF-PRA-EC2-PRI-AMI"
 source_instance_id = aws_instance.pri.id
}















