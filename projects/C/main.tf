module "vpc" {
  source = "/root/Main/modules/vpc"
  cidr   = "10.0.0.0/16"

}
module "subnet" {
  source = "/root/Main/modules/subnet"
  vpc_id = module.vpc.vpc_id
  az     = ["ap-southeast-1b", "ap-southeast-1a"]
  cidr1  = "10.0.0.0/24"
  cidr2  = "10.0.1.0/24"
  cidr3  = "10.0.2.0/24"
  cidr4  = "10.0.3.0/24"
}

module "IGW-RT" {
  source     = "/root/Main/modules/IGW-RT"
  subnet_id  = module.subnet.subnet_id
  subnet2_id = module.subnet.subnet2_id
  subnet3_id = module.subnet.subnet3_id
  subnet4_id = module.subnet.subnet4_id
  table_cidr = "0.0.0.0/0"
  vpc_id     = module.vpc.vpc_id

}

module "ec2" {
  source        = "/root/Main/modules/ec2"
  instance_type = "t2.micro"
  ami           = "ami-05b891753d41ff88f"
  subnet_id     = module.subnet.subnet_id
  subnet3_id    = module.subnet.subnet3_id

  vpc_id = module.vpc.vpc_id

}

module "lb" {
  subnets     = ["${module.subnet.subnet_id}", "${module.subnet.subnet2_id}"]
  source      = "/root/Main/modules/lb"
  interval    = "10"
  protocol    = "HTTP"
  timeout     = "5"
  threshold   = "5"
  unthreshold = "2"
  port        = "80"
  vpc_id      = module.vpc.vpc_id

}

module "asg" {
  source           = "/root/Main/modules/asg"
  subnet_id        = module.subnet.subnet_id
  subnet2_id       = module.subnet.subnet2_id
  subnet3_id       = module.subnet.subnet3_id
  subnet4_id       = module.subnet.subnet4_id
  min_size         = "1"
  desired_capacity = "2"
  max_size         = "4"
  key_name         = "PRA"
  instance_type    = "t2.micro"
  image_id         = module.ec2.image_id
  image1_id        = module.ec2.image1_id
  vpc_id           = module.vpc.vpc_id


}

module "rds" {
  source           = "/root/Main/modules/rds"
  storage          = "256"
  subnet_ids       = ["${module.subnet.subnet3_id}", "${module.subnet.subnet4_id}"]
  retention_period = "7"
  db_username      = "admin"
  db_password      = "password"
  vpc_id           = module.vpc.vpc_id


}


module "privatelb" {
  subnets     = ["${module.subnet.subnet3_id}", "${module.subnet.subnet4_id}"]
  source      = "/root/Main/modules/privet-lb"
  interval    = "10"
  protocol    = "HTTP"
  timeout     = "5"
  threshold   = "5"
  unthreshold = "2"
  port        = "80"
  vpc_id      = module.vpc.vpc_id

}

